import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, Text } from 'react-native';
import { List, ListItem, Button, Icon } from 'react-native-elements';
import firebase from '../../Firebase';

class ColmeiaScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Colmeias',
      headerRight: (
        <Button
          buttonStyle={{ padding: 0, backgroundColor: 'transparent' }}
          icon={{ name: 'add-circle', style: { marginRight: 0, fontSize: 28 } }}
          onPress={() => { navigation.push('AddColmeia') }}
        />
      ),
    };
  };
  constructor() {
    super();
    this.ref = firebase.firestore().collection('colmeias');
    this.unsubscribe = null;
    this.state = {
      isLoading: true,
      colmeias: []
    };
  }
  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }
  onCollectionUpdate = (querySnapshot) => {
    const colmeias = [];
    querySnapshot.forEach((doc) => {
      const { nome, macAddress } = doc.data();
      colmeias.push({
        key: doc.id,
        doc, // DocumentSnapshot
        nome,
        macAddress,
      });
    });
    this.setState({
      colmeias,
      isLoading: false,
    });
  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <List>
          {
            this.state.colmeias.map((item, i) => (
              <ListItem
                key={i}
                title={item.nome}
                leftIcon={{ name: 'bug', type: 'font-awesome' }}
                onPress={() => {
                  this.props.navigation.navigate('ColmeiaDetails', {
                    colmeiakey: `${JSON.stringify(item.key)}`,
                  });
                }}
              />
            ))
          }
        </List>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 22
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default ColmeiaScreen;
