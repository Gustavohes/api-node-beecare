import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, TextInput } from 'react-native';
import { Button } from 'react-native-elements';
import firebase from '../../Firebase';

class AddColmeiaScreen extends Component {
  static navigationOptions = {
    title: 'Adicionar Colmeia',
  };
  constructor() {
    super();
    this.ref = firebase.firestore().collection('colmeias');
    this.state = {
      nome: '',
      macAddress: '',
      isLoading: false,
    };
  }
  updateTextInput = (text, field) => {
    const state = this.state
    state[field] = text;
    this.setState(state);
  }

  saveColmeia() {
    this.setState({
      isLoading: true,
    });
    this.ref.add({
      nome: this.state.nome,
      macAddress: this.state.macAddress,
    }).then((docRef) => {
      this.setState({
        nome: '',
        macAddress: '',
        isLoading: false,
      });
      this.props.navigation.goBack();
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
      this.setState({
        isLoading: false,
      });
    });
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff"/>
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <View style={styles.subContainer}>
          <TextInput
              placeholder={'Nome'}
              value={this.state.nome}
              onChangeText={(text) => this.updateTextInput(text, 'nome')}
          />
        </View>
        <View style={styles.subContainer}>
          <TextInput
              placeholder={'Mac Address'}
              value={this.state.macAddress}
              onChangeText={(text) => this.updateTextInput(text, 'macAddress')}
          />
        </View>
        <View style={styles.button}>
          <Button
            large
            leftIcon={{nome: 'save'}}
            title='Salvar'
            onPress={() => this.saveColmeia()} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  subContainer: {
    flex: 1,
    marginBottom: 20,
    padding: 5,
    borderBottomWidth: 2,
    borderBottomColor: '#CCCCCC',
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default AddColmeiaScreen;
