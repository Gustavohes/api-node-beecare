import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, TextInput } from 'react-native';
import { Button } from 'react-native-elements';
import firebase from '../../Firebase';

class EditColmeiaScreen extends Component {
  static navigationOptions = {
    title: 'Editar Colmeia',
  };
  constructor() {
    super();
    this.state = {
      key: '',
      isLoading: true,
      nome: '',
      macAddress: '',
    };
  }
  componentDidMount() {
    const { navigation } = this.props;
    const ref = firebase.firestore().collection('colmeias').doc(JSON.parse(navigation.getParam('colmeiakey')));
    ref.get().then((doc) => {
      if (doc.exists) {
        const colmeia = doc.data();
        this.setState({
          key: doc.id,
          nome: colmeia.nome,
          macAddress: colmeia.macAddress,
          isLoading: false
        });
      } else {
        console.log("No such document!");
      }
    });
  }

  updateTextInput = (text, field) => {
    const state = this.state
    state[field] = text;
    this.setState(state);
  }

  updateColmeia() {
    this.setState({
      isLoading: true,
    });
    const { navigation } = this.props;
    const updateRef = firebase.firestore().collection('colmeias').doc(this.state.key);
    updateRef.set({
      nome: this.state.nome,
      macAddress: this.state.macAddress,
    }).then((docRef) => {
      this.setState({
        key: '',
        nome: '',
        macAddress: '',
        isLoading: false,
      });
      this.props.navigation.navigate('Colmeia');
    })
      .catch((error) => {
        console.error("Error adding document: ", error);
        this.setState({
          isLoading: false,
        });
      });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <View style={styles.subContainer}>
          <TextInput
            placeholder={'Nome'}
            value={this.state.nome}
            onChangeText={(text) => this.updateTextInput(text, 'nome')}
          />
        </View>
        <View style={styles.subContainer}>
          <TextInput
            placeholder={'Mac Address'}
            value={this.state.macAddress}
            onChangeText={(text) => this.updateTextInput(text, 'macAddress')}
          />
        </View>
        <View style={styles.button}>
          <Button
            large
            buttonStyle={{ backgroundColor: 'green' }}
            leftIcon={{ name: 'update' }}
            title='Update'
            onPress={() => this.updateColmeia()} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  subContainer: {
    flex: 1,
    marginBottom: 20,
    padding: 5,
    borderBottomWidth: 2,
    borderBottomColor: '#CCCCCC',
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default EditColmeiaScreen;
