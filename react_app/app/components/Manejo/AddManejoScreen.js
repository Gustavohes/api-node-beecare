import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, TextInput } from 'react-native';
import { Button } from 'react-native-elements';
import firebase from '../../Firebase';

class AddManejoScreen extends Component {
  static navigationOptions = {
    title: 'Adicionar Manejo',
  };
  constructor() {
    super();
    this.ref = firebase.firestore().collection('manejos');
    this.state = {
      tipoManejo: '',
      colmeia: '',
      data: '',
      isLoading: false,
    };
  }
  updateTextInput = (text, field) => {
    const state = this.state
    state[field] = text;
    this.setState(state);
  }

  saveManejo() {
    this.setState({
      isLoading: true,
    });
    this.ref.add({
      tipoManejo: this.state.tipoManejo,
      colmeia: this.state.colmeia,
      data: this.state.data,
    }).then((docRef) => {
      this.setState({
        tipoManejo: '',
        colmeia: '',
        data: '',
        isLoading: false,
      });
      this.props.navigation.goBack();
    })
      .catch((error) => {
        console.error("Error adding document: ", error);
        this.setState({
          isLoading: false,
        });
      });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <View style={styles.subContainer}>
          <Button
            large
            buttonStyle={{backgroundColor: 'green'}}
            leftIcon={{ name: 'unlock', type: 'font-awesome' }}
            title='Abrir Colmeia'
            onPress={() => this.updateManejo()} />
        </View>
        <View style={styles.subContainer}>
          <Button
            large
            buttonStyle={{backgroundColor: '#f5c242'}}
            leftIcon={{ name: 'lock', type: 'font-awesome' }}
            title='Fechar Colmeia'
            onPress={() => this.updateManejo()} />
        </View>
        <View style={styles.subContainer}>
          <Button
            large
            buttonStyle={{backgroundColor: 'red'}}
            leftIcon={{ name: 'bell', type: 'font-awesome' }}
            title='Tocar Alarme'
            onPress={() => this.updateManejo()} />
        </View>
        {/* <View style={styles.subContainer}>
          <TextInput
            placeholder={'Tipo de Manejo'}
            value={this.state.tipoManejo}
            onChangeText={(text) => this.updateTextInput(text, 'tipoManejo')}
          />
        </View>
        <View style={styles.subContainer}>
          <TextInput
            placeholder={'Colmeia'}
            value={this.state.colmeia}
            onChangeText={(text) => this.updateTextInput(text, 'colmeia')}
          />
        </View>
        <View style={styles.subContainer}>
          <TextInput
            placeholder={'Data'}
            value={this.state.data}
            onChangeText={(text) => this.updateTextInput(text, 'data')}
          />
        </View>
        <View style={styles.button}>
          <Button
            large
            buttonStyle={{backgroundColor: 'green'}}
            leftIcon={{ name: 'save' }}
            title='Save'
            onPress={() => this.saveManejo()} />
        </View> */}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  subContainer: {
    flex: 1,
    marginBottom: 20,
    padding: 5,
    borderBottomWidth: 2,
    borderBottomColor: '#CCCCCC',
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default AddManejoScreen;
