import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View } from 'react-native';
import { List, ListItem, Text, Card, Button } from 'react-native-elements';
import firebase from '../../Firebase';

class ManejoDetailScreen extends Component {
  static navigationOptions = {
    title: 'Detalhes do Manejo',
  };
  constructor() {
    super();
    this.state = {
      isLoading: true,
      manejo: {},
      key: ''
    };
  }
  componentDidMount() {
    const { navigation } = this.props;
    const ref = firebase.firestore().collection('manejos').doc(JSON.parse(navigation.getParam('manejokey')));
    ref.get().then((doc) => {
      if (doc.exists) {
        this.setState({
          manejo: doc.data(),
          key: doc.id,
          isLoading: false
        });
      } else {
        console.log("No such document!");
      }
    });
  }
  deleteManejo(key) {
    const { navigation } = this.props;
    this.setState({
      isLoading: true
    });
    firebase.firestore().collection('manejos').doc(key).delete().then(() => {
      console.log("Document successfully deleted!");
      this.setState({
        isLoading: false
      });
      navigation.navigate('Manejo');
    }).catch((error) => {
      console.error("Error removing document: ", error);
      this.setState({
        isLoading: false
      });
    });
  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )
    }
    return (
      <ScrollView>
        <Card style={styles.container}>
          <View style={styles.subContainer}>
            <View>
              <Text h3>{this.state.manejo.tipoManejo}</Text>
            </View>
            <View>
              <Text h5>{this.state.manejo.colmeia}</Text>
            </View>
            <View>
              <Text h4>{this.state.manejo.data}</Text>
            </View>
          </View>
          <View style={styles.detailButton}>
            <Button
              large
              buttonStyle={{ backgroundColor: 'green' }}
              leftIcon={{ name: 'edit' }}
              title='Edit'
              onPress={() => {
                this.props.navigation.navigate('EditManejo', {
                  manejokey: `${JSON.stringify(this.state.key)}`,
                });
              }} />
          </View>
          <View style={styles.detailButton}>
            <Button
              large
              buttonStyle={{ backgroundColor: 'red' }}
              color={'#FFFFFF'}
              leftIcon={{ name: 'delete' }}
              title='Delete'
              onPress={() => this.deleteManejo(this.state.key)} />
          </View>
        </Card>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  subContainer: {
    flex: 1,
    paddingBottom: 20,
    borderBottomWidth: 2,
    borderBottomColor: '#CCCCCC',
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  detailButton: {
    marginTop: 10
  }
})

export default ManejoDetailScreen;
