import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, TextInput } from 'react-native';
import { Button } from 'react-native-elements';
import firebase from '../../Firebase';

class EditManejoScreen extends Component {
  static navigationOptions = {
    title: 'Editar Manejo',
  };
  constructor() {
    super();
    this.state = {
      key: '',
      isLoading: true,
      tipoManejo: '',
      colmeia: '',
      data: ''
    };
  }
  componentDidMount() {
    const { navigation } = this.props;
    const ref = firebase.firestore().collection('manejos').doc(JSON.parse(navigation.getParam('manejokey')));
    ref.get().then((doc) => {
      if (doc.exists) {
        const manejo = doc.data();
        this.setState({
          key: doc.id,
          tipoManejo: manejo.tipoManejo,
          colmeia: manejo.colmeia,
          data: manejo.data,
          isLoading: false
        });
      } else {
        console.log("No such document!");
      }
    });
  }

  updateTextInput = (text, field) => {
    const state = this.state
    state[field] = text;
    this.setState(state);
  }

  updateManejo() {
    this.setState({
      isLoading: true,
    });
    const { navigation } = this.props;
    const updateRef = firebase.firestore().collection('manejos').doc(this.state.key);
    updateRef.set({
      tipoManejo: this.state.tipoManejo,
      colmeia: this.state.colmeia,
      data: this.state.data,
    }).then((docRef) => {
      this.setState({
        key: '',
        tipoManejo: '',
        colmeia: '',
        data: '',
        isLoading: false,
      });
      this.props.navigation.navigate('Manejo');
    })
      .catch((error) => {
        console.error("Error adding document: ", error);
        this.setState({
          isLoading: false,
        });
      });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <View style={styles.subContainer}>
          <TextInput
            placeholder={'Tipo de Manejo'}
            value={this.state.tipoManejo}
            onChangeText={(text) => this.updateTextInput(text, 'tipoManejo')}
          />
        </View>
        <View style={styles.subContainer}>
          <TextInput
            placeholder={'Colmeia'}
            value={this.state.colmeia}
            onChangeText={(text) => this.updateTextInput(text, 'colmeia')}
          />
        </View>
        <View style={styles.subContainer}>
          <TextInput
            placeholder={'Data'}
            value={this.state.data}
            onChangeText={(text) => this.updateTextInput(text, 'data')}
          />
        </View>
        <View style={styles.button}>
          <Button
            large
            buttonStyle={{backgroundColor: 'green'}}
            leftIcon={{ name: 'update' }}
            title='Update'
            onPress={() => this.updateManejo()} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  subContainer: {
    flex: 1,
    marginBottom: 20,
    padding: 5,
    borderBottomWidth: 2,
    borderBottomColor: '#CCCCCC',
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default EditManejoScreen;
