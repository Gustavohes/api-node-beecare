import React, { Component } from 'react';
import { StyleSheet, ScrollView, ActivityIndicator, View, Text } from 'react-native';
import { List, ListItem, Button, Icon } from 'react-native-elements';
import firebase from '../../Firebase';

class ManejoScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Manejos',
      headerRight: (
        <Button
          buttonStyle={{ padding: 0, backgroundColor: 'transparent' }}
          icon={{ name: 'add', style: { marginRight: 0, fontSize: 28 } }}
          onPress={() => { navigation.push('AddManejo') }}
        />
      ),
    };
  };
  constructor() {
    super();
    this.ref = firebase.firestore().collection('manejos');
    this.unsubscribe = null;
    this.state = {
      isLoading: true,
      manejos: []
    };
  }
  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }
  onCollectionUpdate = (querySnapshot) => {
    const manejos = [];
    querySnapshot.forEach((doc) => {
      const { tipoManejo, colmeia, data } = doc.data();
      manejos.push({
        key: doc.id,
        doc, // DocumentSnapshot
        tipoManejo,
        colmeia,
        data,
      });
    });
    this.setState({
      manejos,
      isLoading: false,
   });
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.activity}>
          <ActivityIndicator size="large" color="#0000ff"/>
        </View>
      )
    }
    return (
      <ScrollView style={styles.container}>
        <List>
          {
            this.state.manejos.map((item, i) => (
              <ListItem
                key={i}
                title={item.tipoManejo}
                leftIcon={{name: 'magic', type: 'font-awesome'}}
                onPress={() => {
                  this.props.navigation.navigate('ManejoDetails', {
                    manejokey: `${JSON.stringify(item.key)}`,
                  });
                }}
              />
            ))
          }
        </List>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingBottom: 22
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default ManejoScreen;
