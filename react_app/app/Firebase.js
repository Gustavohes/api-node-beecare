import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const settings = {timestampsInSnapshots: true};

const config = {
  apiKey: "AIzaSyBCq4feDmLXkhmqgsfMt2sFivPgtlYghs4",
  authDomain: "https://crudreactnative-4fbf8.firebaseio.com",
  databaseURL: "https://crudreactnative-4fbf8.firebaseio.com",
  projectId: "crudreactnative-4fbf8",
  storageBucket: "crudreactnative-4fbf8.appspot.com",
  messagingSenderId: "708246498617"
};
firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;
