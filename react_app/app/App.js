import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator, createDrawerNavigator } from 'react-navigation';
import ManejoScreen from './components/Manejo/ManejoScreen';
import ManejoDetailScreen from './components/Manejo/ManejoDetailScreen';
import AddManejoScreen from './components/Manejo/AddManejoScreen';
import EditManejoScreen from './components/Manejo/EditManejoScreen';
import ColmeiaScreen from './components/Colmeia/ColmeiaScreen';
import ColmeiaDetailScreen from './components/Colmeia/ColmeiaDetailScreen';
import AddColmeiaScreen from './components/Colmeia/AddColmeiaScreen';
import EditColmeiaScreen from './components/Colmeia/EditColmeiaScreen';

const ManejoStackNavigator = createStackNavigator(
  {
    Manejo: ManejoScreen,
    ManejoDetails: ManejoDetailScreen,
    AddManejo: AddManejoScreen,
    EditManejo: EditManejoScreen,
  },
  {
    initialRouteName: 'Manejo',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#353535',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerBackTitle: null,
    },
  },
);

const ColmeiaStackNavigator = createStackNavigator(
  {
    Colmeia: ColmeiaScreen,
    ColmeiaDetails: ColmeiaDetailScreen,
    AddColmeia: AddColmeiaScreen,
    EditColmeia: EditColmeiaScreen,
  },
  {
    initialRouteName: 'Colmeia',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#353535',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerBackTitle: null,
    },
  },
);

const AppNavigator = createDrawerNavigator({
  Manejo: { screen: ManejoScreen },
  Colmeia: { screen: ColmeiaScreen },
},
  {
    initialRouteName: 'Manejo',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#353535',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerBackTitle: null,
    },
  })


export default class App extends React.Component {
  render() {
    return <ColmeiaStackNavigator />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
