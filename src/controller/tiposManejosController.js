const express = require('express');
const bodyParser = require('body-parser');

const con = require('../../server');

const app = express();

app.use(bodyParser.json());

module.exports = {
    index (req,res) {
        let sql = "SELECT * FROM tiposmanejos";
        con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });      
    },

    show (req, res) { 
        let filter = '';
        if(req.params.id) filter = ' WHERE id=' + parseInt(req.params.id);
        let sql = "SELECT * FROM tiposmanejos";
        let query = (sql + filter);
        con.query(query, (err,results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },
      
    store (req, res) {
        let id = req.params.id;
        let description = req.body.descricao;
        let sql=`INSERT INTO tiposmanejos (descricao) VALUES('${description}')`;
        con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },
      
    update (req, res) {
        let id = req.params.id;
        let description = req.body.descricao;
        let sql = `UPDATE tiposmanejos SET descricao='${description}' WHERE Id=${id}`;
        con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },

    destroy (req, res) {
        let filter = '';
        if(req.params.id) filter = ' WHERE id=' + parseInt(req.params.id);
        let sql = "DELETE FROM tiposmanejos";
        let query = (sql + filter);
        con.query(query, (err,results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },
};
