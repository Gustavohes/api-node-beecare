const express = require('express');
const bodyParser = require('body-parser');

const con = require('../../server');

const app = express();

app.use(bodyParser.json());

module.exports = {
    index (req,res) {
        let sql = "SELECT p.nome, pr.cnpj, pr.nomeFantasia FROM proprietario pr, pessoas p WHERE p.id = pr.IdPessoa";
        con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });      
    },

    show (req, res) {
        let filter = '';
        if(req.params.id) filter = ' WHERE p.id = pr.IdPessoa AND IdPessoa=' + parseInt(req.params.id);
        let sql = "SELECT p.nome, pr.cnpj, pr.nomeFantasia FROM proprietario pr, pessoas p";
        let query = (sql + filter);
        con.query(query, (err,results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },
      
    store (req, res) {
        let id = req.params.id;
        let cnpj = req.body.cnpj;
        let nomeFantasia = req.body.nomeFantasia;
        let sql=`INSERT INTO proprietario (IdPessoa, cnpj, nomeFantasia) VALUES((SELECT id FROM pessoas WHERE id =${id}), '${cnpj}', '${nomeFantasia}')`;
        con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },
      
    update (req, res) {
        let idPerson = req.params.id;
        let cnpj = req.body.cnpj;
        let nomeFantasia = req.body.nomeFantasia;
        let sql = `UPDATE proprietario SET cnpj='${cnpj}', nomeFantasia='${nomeFantasia}' WHERE IdPessoa=${idPerson}`;
        con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },

    destroy (req, res) {
        let filter = '';
        if(req.params.id) filter = ' WHERE IdPessoa=' + parseInt(req.params.id);
        let sql = "DELETE FROM proprietario";
        let query = (sql + filter);
        con.query(query, (err,results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },
};
