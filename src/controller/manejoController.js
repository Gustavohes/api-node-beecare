const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');

const con = require('../../server');

const app = express();

app.use(bodyParser.json());

module.exports = {
//TODO adcionar os campos na tabela de manejo: nomeManejo e Descricao Manejo


/*     SELECT m.STATUS,
		 m.dataHora,
		 pp.nome AS Funcionario,
		 p.nome AS Proprietario,
		 c.nome AS Colmeias
        FROM manejos m
        LEFT JOIN pessoas pp ON pp.id = m.idFuncionario 
        LEFT JOIN pessoas p ON p.id = m.idProprietario 
        LEFT JOIN colmeias c ON c.id = m.idColmeia */
        index (req,res) {
        let sql = "SELECT m.STATUS, m.dataHora, pp.nome AS Funcionario, p.nome AS Proprietario, c.nome AS Colmeias FROM manejos m LEFT JOIN pessoas pp ON pp.id = m.idFuncionario LEFT JOIN pessoas p ON p.id = m.idProprietario LEFT JOIN colmeias c ON c.id = m.idColmeia";
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });      
      },
      
      show (req, res) {
        let filter = '';
        if(req.params.id) filter = ' WHERE m.idColmeia = c.id AND m.id=' + parseInt(req.params.id);
        let sql = "SELECT m.status, m.dataHora, (SELECT p.nome FROM pessoas p, manejos m WHERE m.idFuncionario = p.id AND m.id =" + parseInt(req.params.id) + ") AS Funcionario, (SELECT p.nome FROM pessoas p, manejos m WHERE m.idProprietario = p.id AND m.id =" + parseInt(req.params.id) + ") AS Proprietario, c.nome FROM manejos m, colmeias c";
        let query = (sql + filter);
        let query1 = con.query(query, (err,results) => {
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },
      
      store (req, res) {
        let status = req.body.status;
        let dateTime = req.body.dateTime;
        let idFuncionario = req.body.idFuncionario;
        let idProprietario = req.body.idProprietario;
        let idColmeia = req.body.idColmeia;
        let sql = `INSERT INTO manejos(status, dataHora, idFuncionario, idProprietario, idColmeia) VALUES('${status}','${dateTime}','${idFuncionario}','${idProprietario}','${idColmeia}')`;
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },
      
      update (req, res) {
        let manejoId = req.params.id;
        let status = req.body.status;
        let dateTime = req.body.dateTime;
        let idFuncionario = req.params.idFuncionario;
        let idProprietario = req.params.idProprietario;
        let idColmeia = req.params.idColmeia;
        let sql = `UPDATE manejos SET status='${status}', descricao='${dateTime}', idFuncionario='${idFuncionario}' ,idProprietario='${idProprietario}' ,idColmeia='${idColmeia}' WHERE id=${manejoId}`;
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },

      destroy (req, res) {
      let filter = '';
      if(req.params.id) filter = ' WHERE ID=' + parseInt(req.params.id);
      let sql = "DELETE FROM manejos";
      let query = (sql + filter);
      let query1 = con.query(query, (err,results) => {
      if(err) throw err;
      res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
      });
    },

    fecharColmeia(req,res){
      res.send(JSON.stringify({"closeBeehive":1}));
    },

    desligarAlarme(res){
      res.send(JSON.stringify({"soundOffAlarm":0}));
    },

    apagarLuz(res){
      res.send(JSON.stringify({"turnOffLed":0}));
    },


    AbrirColmeia(res){
      res.send(JSON.stringify({"openBeehive":1}));
    },

    ligarAlarme(res){
      res.send(JSON.stringify({"soundOnAlarm":1}));
    },

    AcenderLuz(res){
      res.send(JSON.stringify({"turnOnLed":1}));
    },
};