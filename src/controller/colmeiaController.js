const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');

const con = require('../../server');

const app = express();

app.use(bodyParser.json());

module.exports = {
      index (req,res) {
//         res.send('sasdasd');
       let sql = "SELECT * FROM colmeias";
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });      
      },

      show (req, res) {
        let filter = '';
        if(req.params.id) filter = ' WHERE ID=' + parseInt(req.params.id);
        let sql = "SELECT * FROM colmeias";
        let query = (sql + filter);
        let query1 = con.query(query, (err,results) => {
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },
      
      store (req, res) {
        let name = req.body.name;
        let description = req.body.description;
        let status = req.body.status;
        let sql = `INSERT INTO colmeias(nome, descricao, status) VALUES('${name}','${description}','${status}')`;
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },
      
      update (req, res) {
        let colmeiaId = req.params.id;
        let name = req.body.name;
        let description = req.body.description;
        let status = req.body.status;
        let sql = `UPDATE colmeias SET nome='${name}', descricao='${description}', status='${status}' WHERE id=${colmeiaId}`;
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },

      destroy (req, res) {
      let filter = '';
      if(req.params.id) filter = ' WHERE ID=' + parseInt(req.params.id);
      let sql = "DELETE FROM colmeias";
      let query = (sql + filter);
      let query1 = con.query(query, (err,results) => {
      if(err) throw err;
      res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
      });
    },
};

