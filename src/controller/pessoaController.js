const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');

const con = require('../../server');

const app = express();

app.use(bodyParser.json());

module.exports = {
      index (req,res) {
        let sql = "SELECT * FROM pessoas";
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });      
      },

      show (req, res) {
        let filter = '';
        if(req.params.id) filter = ' WHERE ID=' + parseInt(req.params.id);
        let sql = "SELECT * FROM pessoas";
        let query = (sql + filter);
        let query1 = con.query(query, (err,results) => {
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },
      
      store (req, res) {
        let user = req.body.user;
        let senha = req.body.senha;
        let nome = req.body.nome;
        let sql = `INSERT INTO pessoas(usuario, senha, nome) VALUES('${user}','${senha}','${nome}')`;
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },
      
      update (req, res) {
        let pessoaId = req.params.id;
        let user = req.body.user;
        let senha = req.body.senha;
        let nome = req.body.nome;
        let sql = `UPDATE pessoas SET usuario='${user}', senha='${senha}', nome='${nome}' WHERE id=${pessoaId}`;
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },

      destroy (req, res) {
      let filter = '';
      if(req.params.id) filter = ' WHERE ID=' + parseInt(req.params.id);
      let sql = "DELETE FROM pessoas";
      let query = (sql + filter);
      let query1 = con.query(query, (err,results) => {
      if(err) throw err;
      res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
      });
    },
};

