const express = require('express');
const bodyParser = require('body-parser');

const con = require('../../server');

const app = express();

app.use(bodyParser.json());

module.exports = {
      index (req,res) {
        let sql = "SELECT nome, salario FROM funcionarios f, pessoas p WHERE p.id = f.IdPessoa";
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });      
      },

      show (req, res) {
        let filter = '';
        if(req.params.id) filter = ' WHERE p.id = f.IdPessoa AND IdPessoa=' + parseInt(req.params.id);
        let sql = "SELECT nome, salario FROM funcionarios f, pessoas p";
        let query = (sql + filter);
        con.query(query, (err,results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },
      
      store (req, res) {
        let id = req.params.id;
        let salary = req.body.salario;
        let sql=`INSERT INTO funcionarios (IdPessoa, salario) VALUES((SELECT id FROM pessoas WHERE id =${id}), ${salary})`;
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },
      
      update (req, res) {
        let idPerson = req.params.id;
        let salary = req.body.salario;
        let sql = `UPDATE funcionarios SET salario=${salary} WHERE IdPessoa=${idPerson}`;
        let query = con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
      },

      destroy (req, res) {
      let filter = '';
      if(req.params.id) filter = ' WHERE IdPessoa=' + parseInt(req.params.id);
      let sql = "DELETE FROM funcionarios";
      let query = (sql + filter);
      let query1 = con.query(query, (err,results) => {
      if(err) throw err;
      res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
      });
    },
};
