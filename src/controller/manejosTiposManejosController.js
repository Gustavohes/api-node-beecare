const express = require('express');
const bodyParser = require('body-parser');

const con = require('../../server');

const app = express();

app.use(bodyParser.json());

module.exports = {
    //TODO adcionar a tabela de manejo no select
    index (req,res) {
        let sql = "SELECT tm.descricao, p.nome, mtm.dataHora FROM manejos_tipoManejos mtm, tiposManejos tm, pessoas p WHERE tm.id = mtm.idTipoManejos AND p.id = mtm.idPessoa";
        con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });      
    },

    show (req, res) { 
        let idManagement = req.params.idManejo;
        let idTypeManagement = req.params.idTipoManejos;
        let idPerson = req.params.idPessoa;
        let filter = '';
        if(idManagement && idTypeManagement && idPerson) filter = ` WHERE idManejo ='${idManagement}' AND IdTipoManejos='${idTypeManagement}' AND idPessoa='${idPerson}'`; 
        let sql = "SELECT * FROM manejos_tipomanejos";
        let query = (sql + filter);
        con.query(query, (err,results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },
      
    store (req, res) {
        let idManagement = req.body.idManejo;
        let idTypeManagement = req.body.idTipoManejos;
        let idPerson = req.body.idPessoa;
        let dateTime = req.body.dataHora;
        let sql=`INSERT INTO manejos_tipomanejos (idManejo, IdTipoManejos, idPessoa, dataHora) VALUES((SELECT id FROM manejos WHERE id =${idManagement}), (SELECT id FROM tiposmanejos WHERE id =${idTypeManagement}), (SELECT id FROM pessoas WHERE id =${idPerson}), '${dateTime}' )`;
        con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },

    update (req, res) {
        let idManagement = req.params.idManejo;
        let idTypeManagement = req.params.idTipoManejos;
        let idPerson = req.params.idPessoa;
        let dateTime = req.body.dataHora;
        let sql = `UPDATE manejos_tipomanejos SET dataHora='${dateTime}' WHERE IdManejo=${idManagement} AND IdTipoManejos=${idTypeManagement} AND IdPessoa=${idPerson}`;
        con.query(sql, (err, results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },

    destroy (req, res) {
        let idManagement = req.params.idManejo;
        let idTypeManagement = req.params.idTipoManejos;
        let idPerson = req.params.idPessoa;
        let filter = ''; 
        if(idManagement && idTypeManagement && idPerson) filter = ` WHERE idManejo ='${idManagement}' AND IdTipoManejos='${idTypeManagement}' AND idPessoa='${idPerson}' ` ;
        let sql = "DELETE FROM manejos_tipomanejos";
        let query = (sql + filter);
        con.query(query, (err,results) => {
        if(err) throw err;
        res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        });
    },
};
