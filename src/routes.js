const express = require('express');
const routes = express.Router();

const pessoaController = require('./controller/pessoaController');
routes.get('/pessoas', pessoaController.index);
routes.get('/pessoas/:id?', pessoaController.show);
routes.post('/pessoas', pessoaController.store);
routes.put('/pessoas/:id?', pessoaController.update);
routes.delete('/pessoas/:id?', pessoaController.destroy);

const colmeiaController = require('./controller/colmeiaController');
routes.get('/colmeias', colmeiaController.index);
routes.get('/colmeias/:id?', colmeiaController.show);
routes.post('/colmeias', colmeiaController.store);
routes.put('/colmeias/:id?', colmeiaController.update);
routes.delete('/colmeias/:id?', colmeiaController.destroy);

const manejoController = require('./controller/manejoController');
routes.get('/manejos', manejoController.index);
routes.get('/manejos/:id?', manejoController.show);
routes.post('/manejos', manejoController.store);
routes.put('/manejos/:id?idFuncionario=:idFuncionario&idProprietario=:idProprietario&idColmeia=:idColmeia', manejoController.update);
routes.delete('/manejos/:id?', manejoController.destroy);

routes.get('/fecharColmeia', manejoController.fecharColmeia);
routes.get('/desligarAlarme', manejoController.desligarAlarme);
routes.get('/apagarLuz', manejoController.apagarLuz);
routes.get('/AbrirColmeia', manejoController.AbrirColmeia);
routes.get('/ligarAlarme', manejoController.ligarAlarme);
routes.get('/AcenderLuz', manejoController.AcenderLuz);

const funcionarioController = require('./controller/funcionarioController');
routes.get('/funcionario', funcionarioController.index);
routes.get('/funcionario/:id?', funcionarioController.show);
routes.post('/funcionario/:id?', funcionarioController.store);
routes.put('/funcionario/:id?', funcionarioController.update);
routes.delete('/funcionario/:id?', funcionarioController.destroy);

const proprietarioController = require('./controller/proprietarioController');
routes.get('/proprietario', proprietarioController.index);
routes.get('/proprietario/:id?', proprietarioController.show);
routes.post('/proprietario/:id?', proprietarioController.store);
routes.put('/proprietario/:id?', proprietarioController.update);
routes.delete('/proprietario/:id?', proprietarioController.destroy);

const ocorrenciasController = require('./controller/ocorrenciasController');
routes.get('/ocorrencias', ocorrenciasController.index);
routes.get('/ocorrencias/:id?', ocorrenciasController.show);
routes.post('/ocorrencias/:id?', ocorrenciasController.store);
routes.put('/ocorrencias/:id?', ocorrenciasController.update);
routes.delete('/ocorrencias/:id?', ocorrenciasController.destroy);

const tiposManejosController = require('./controller/tiposManejosController');
routes.get('/tiposManejos', tiposManejosController.index);
routes.get('/tiposManejos/:id?', tiposManejosController.show);
routes.post('/tiposManejos', tiposManejosController.store);
routes.put('/tiposManejos/:id?', tiposManejosController.update);
routes.delete('/tiposManejos/:id?', tiposManejosController.destroy);

const manejosTiposManejosController = require('./controller/manejosTiposManejosController');
routes.get('/manejosTiposManejos', manejosTiposManejosController.index);
routes.get('/manejosTiposManejos/:idManejo/:idTipoManejos/:idPessoa', manejosTiposManejosController.show);
routes.post('/manejosTiposManejos', manejosTiposManejosController.store);
routes.put('/manejosTiposManejos/:idManejo/:idTipoManejos/:idPessoa', manejosTiposManejosController.update);
routes.delete('/manejosTiposManejos/:idManejo/:idTipoManejos/:idPessoa', manejosTiposManejosController.destroy);

module.exports = routes;
