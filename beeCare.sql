-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.6-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para nodeapi
CREATE DATABASE IF NOT EXISTS `nodeapi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `nodeapi`;

-- Copiando estrutura para tabela nodeapi.colmeias
CREATE TABLE IF NOT EXISTS `colmeias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(500) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela nodeapi.colmeias: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `colmeias` DISABLE KEYS */;
INSERT IGNORE INTO `colmeias` (`id`, `nome`, `descricao`, `status`) VALUES
	(1, 'Cultura de espécie Apis Mellifera Scutellata', 'A abelha africana é uma subespécie de abelha ocidental. É nativa do centro e sul da África, embora n', 1),
	(2, 'Cultura de espécie Apis mellifera ligustica', 'A abelha italiana é uma abelha social, originária do Sul da Europa, cujas operárias medem de 12 mm a 13 mm de comprimento e apresentam faixas amarelas nos segmentos abdominais. Também é conhecida como abelha-amarela e abelha-italiana-amarela.', 2),
	(4, 'Abelha-africanizada alterada', 'Colmeia de Abelhas Africanas aaa   sssa', 1);
/*!40000 ALTER TABLE `colmeias` ENABLE KEYS */;

-- Copiando estrutura para tabela nodeapi.funcionarios
CREATE TABLE IF NOT EXISTS `funcionarios` (
  `IdPessoa` int(11) NOT NULL,
  `salario` decimal(10,2) NOT NULL,
  PRIMARY KEY (`IdPessoa`),
  CONSTRAINT `FK_funcionarios_pessoas` FOREIGN KEY (`IdPessoa`) REFERENCES `pessoas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela nodeapi.funcionarios: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `funcionarios` DISABLE KEYS */;
INSERT IGNORE INTO `funcionarios` (`IdPessoa`, `salario`) VALUES
	(1, 2400.50),
	(2, 3500.50),
	(3, 50000.00);
/*!40000 ALTER TABLE `funcionarios` ENABLE KEYS */;

-- Copiando estrutura para tabela nodeapi.manejos
CREATE TABLE IF NOT EXISTS `manejos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `dataHora` datetime NOT NULL,
  `idFuncionario` int(11) NOT NULL,
  `idProprietario` int(11) NOT NULL,
  `idColmeia` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Manejos_proprietario` (`idProprietario`),
  KEY `FK_Manejos_funcionarios` (`idFuncionario`),
  KEY `FK_Manejos_colmeias` (`idColmeia`),
  CONSTRAINT `FK_Manejos_colmeias` FOREIGN KEY (`idColmeia`) REFERENCES `colmeias` (`id`),
  CONSTRAINT `FK_Manejos_funcionarios` FOREIGN KEY (`idFuncionario`) REFERENCES `funcionarios` (`IdPessoa`),
  CONSTRAINT `FK_Manejos_proprietario` FOREIGN KEY (`idProprietario`) REFERENCES `proprietario` (`IdPessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela nodeapi.manejos: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `manejos` DISABLE KEYS */;
INSERT IGNORE INTO `manejos` (`id`, `status`, `dataHora`, `idFuncionario`, `idProprietario`, `idColmeia`) VALUES
	(1, 1, '0000-00-00 00:00:00', 2, 1, 1),
	(2, 2, '0000-00-00 00:00:00', 1, 1, 2);
/*!40000 ALTER TABLE `manejos` ENABLE KEYS */;

-- Copiando estrutura para tabela nodeapi.manejos_tipomanejos
CREATE TABLE IF NOT EXISTS `manejos_tipomanejos` (
  `idManejo` int(11) NOT NULL,
  `IdTipoManejos` int(11) NOT NULL,
  `idPessoa` int(11) NOT NULL,
  `dataHora` datetime NOT NULL,
  PRIMARY KEY (`idManejo`,`IdTipoManejos`,`idPessoa`),
  KEY `FK_Manejos_TipoManejos_tiposmanejos` (`IdTipoManejos`),
  KEY `FK_Manejos_Pessoas` (`idPessoa`),
  CONSTRAINT `FK_Manejos_Pessoas` FOREIGN KEY (`idPessoa`) REFERENCES `pessoas` (`id`),
  CONSTRAINT `FK_Manejos_TipoManejos_manejos` FOREIGN KEY (`idManejo`) REFERENCES `manejos` (`id`),
  CONSTRAINT `FK_Manejos_TipoManejos_tiposmanejos` FOREIGN KEY (`IdTipoManejos`) REFERENCES `tiposmanejos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela nodeapi.manejos_tipomanejos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `manejos_tipomanejos` DISABLE KEYS */;
INSERT IGNORE INTO `manejos_tipomanejos` (`idManejo`, `IdTipoManejos`, `idPessoa`, `dataHora`) VALUES
	(1, 2, 1, '1988-10-21 13:28:06'),
	(1, 2, 2, '1999-10-21 13:28:06'),
	(2, 1, 1, '2077-10-21 13:28:06');
/*!40000 ALTER TABLE `manejos_tipomanejos` ENABLE KEYS */;

-- Copiando estrutura para tabela nodeapi.ocorrencias
CREATE TABLE IF NOT EXISTS `ocorrencias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL,
  `idColmeia` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Ocorrencias_colmeias` (`idColmeia`),
  CONSTRAINT `FK_Ocorrencias_colmeias` FOREIGN KEY (`idColmeia`) REFERENCES `colmeias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela nodeapi.ocorrencias: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `ocorrencias` DISABLE KEYS */;
INSERT IGNORE INTO `ocorrencias` (`id`, `descricao`, `idColmeia`) VALUES
	(1, 'Evento Normativo, Coleta de Mel para produção', 2),
	(2, 'Evento Cansativo, Coleta de Mel para Chatiassção', 2),
	(4, 'Evento Atipico, detecção de gás, letal, teor neocotinoide-30%', 2),
	(5, 'Evento Atipico, detecção de gás, letal, teor neocotinoide-30%', 1),
	(6, 'Evento Atipico, detecção de gás, letal, teor neocotinoide-30%', 1),
	(7, 'Evento Atipico, detecção de gás, letal, teor neocotinoide-30%', 1);
/*!40000 ALTER TABLE `ocorrencias` ENABLE KEYS */;

-- Copiando estrutura para tabela nodeapi.pessoas
CREATE TABLE IF NOT EXISTS `pessoas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) NOT NULL,
  `senha` varchar(1024) NOT NULL,
  `nome` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela nodeapi.pessoas: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `pessoas` DISABLE KEYS */;
INSERT IGNORE INTO `pessoas` (`id`, `usuario`, `senha`, `nome`) VALUES
	(1, 'gustavo@gustavo', '123', 'gustavo'),
	(2, 'simeao@simeao', '123', 'simeao'),
	(3, 'alisson@Flamenguista', 'mengao123456', 'alisson');
/*!40000 ALTER TABLE `pessoas` ENABLE KEYS */;

-- Copiando estrutura para tabela nodeapi.produtos
CREATE TABLE IF NOT EXISTS `produtos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) NOT NULL DEFAULT '0',
  `Preco` varchar(50) NOT NULL DEFAULT '0',
  `Descricao` varchar(50) DEFAULT '0',
  `DataCriado` date NOT NULL DEFAULT '0000-00-00',
  `DataAlterado` date NOT NULL DEFAULT '0000-00-00',
  `Válido` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela nodeapi.produtos: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `produtos` DISABLE KEYS */;
INSERT IGNORE INTO `produtos` (`ID`, `Nome`, `Preco`, `Descricao`, `DataCriado`, `DataAlterado`, `Válido`) VALUES
	(1, 'gtx1070', '500', 'blá blá blá', '2013-04-25', '2013-04-25', 1),
	(2, 'gtx1070', '500', 'blá blá blá', '2013-04-25', '2019-04-25', 1),
	(3, 'gtx1070', '500', 'blá blá blá', '2013-04-25', '2019-04-25', 1),
	(4, 'gtx1050', '250', 'blá blá blá blá', '2013-04-25', '2017-07-27', 1);
/*!40000 ALTER TABLE `produtos` ENABLE KEYS */;

-- Copiando estrutura para tabela nodeapi.proprietario
CREATE TABLE IF NOT EXISTS `proprietario` (
  `IdPessoa` int(11) NOT NULL,
  `cnpj` varchar(20) NOT NULL,
  `nomeFantasia` varchar(100) NOT NULL,
  PRIMARY KEY (`IdPessoa`),
  CONSTRAINT `FK_proprietario_pessoas` FOREIGN KEY (`IdPessoa`) REFERENCES `pessoas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela nodeapi.proprietario: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `proprietario` DISABLE KEYS */;
INSERT IGNORE INTO `proprietario` (`IdPessoa`, `cnpj`, `nomeFantasia`) VALUES
	(1, '47507571874', 'BeeCare'),
	(2, '3154784655', 'Care it OUT'),
	(3, '465128495', ' Bee Hive Masters 2');
/*!40000 ALTER TABLE `proprietario` ENABLE KEYS */;

-- Copiando estrutura para tabela nodeapi.tiposmanejos
CREATE TABLE IF NOT EXISTS `tiposmanejos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela nodeapi.tiposmanejos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tiposmanejos` DISABLE KEYS */;
INSERT IGNORE INTO `tiposmanejos` (`id`, `descricao`) VALUES
	(1, 'Retirada de mel do coletor'),
	(2, 'Borrifação de hormonio Alterado');
/*!40000 ALTER TABLE `tiposmanejos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


/* 
  INSERT INTO ocorrencias (descricao, idColmeia) VALUES('Evento Atipico, detecção de gás, não-letal', 2)

SELECT * FROM manejos


SELECT m.STATUS,
		 m.dataHora,
		 pp.nome AS Funcionario,
		 p.nome AS Proprietario,
		 c.nome AS Colmeias
FROM manejos m
LEFT JOIN pessoas pp ON pp.id = m.idFuncionario 
LEFT JOIN pessoas p ON p.id = m.idProprietario 
LEFT JOIN colmeias c ON c.id = m.idColmeia
		
------------------------------------------------------------------------------------------------

SELECT m.STATUS,
		 m.dataHora,
		 (SELECT p.nome FROM pessoas p, manejos m WHERE m.idFuncionario = p.id AND m.id = 1) AS Funcionario,
		 (SELECT p.nome FROM pessoas p, manejos m WHERE m.idProprietario = p.id AND m.id = 1) AS Proprietario, 
		 c.nome 
FROM manejos m, 
	  colmeias c
WHERE m.idColmeia = c.id AND
		m.id = 1

------------------------------------------------------------------------------------------------

SELECT m.STATUS,
		 m.dataHora,
		 (SELECT p.nome FROM pessoas p, manejos m WHERE m.idFuncionario = p.id) AS Funcionario,
		 (SELECT pp.nome FROM pessoas pp, manejos m WHERE m.idProprietario = pp.id) AS Proprietario, 
		 c.nome 
FROM manejos m, 
	  colmeias c
WHERE m.idColmeia = c.id
		
SELECT nome FROM funcionarios f, pessoas p WHERE p.id = f.IdPessoa

SELECT p.nome FROM pessoas p, manejos m WHERE m.idProprietario = p.id
 */
