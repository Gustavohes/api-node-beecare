const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');

//inicio App
const app = express();
app.use(bodyParser.json());

const con = mysql.createConnection({
    host:'127.0.0.1',
    user:'root',
    password: '',
    port: '3306',
    database: 'nodeapi'
});

con.connect((err) =>{
    if(err) throw err;
    console.log('conectado!');
});

module.exports = con;

//con.end();
//const pool = mysql.createPool(con);
//module.exports = pool;

//Rotas
app.use('/', require('./src/routes'));

app.listen(3010, '192.168.0.105');